AUTHOR: Sergey Kirakosyan
ABOUT: This project will be used to calculate the Work using integral numerical methods in Python.
PREREQUISITES: Firstly it was important to decide which numerical method would be used to calculate the work. Secondly It was very important to decide how to implement this numerical method in Python. Desription of the numerical method used in the program and the implementation can be found in the pdf file and main.py file respectivelly.
USAGE: This program will be used by students who will need to calculate work by numerical methods or using coservative forces during their Engineering Dynamics class.
