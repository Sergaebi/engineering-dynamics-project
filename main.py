import argparse
import os
import psutil


class Body:
    """This class describes the body
    which is affected by some force"""

    def __init__(self, choice, x1, x2, y1, y2):
        self.y_choice = choice
        self.x_initial = x1
        self.x_final = x2
        self.y_initial = y1
        self.y_final = y2
        self.force_function = self.getting_function_input()

    def getting_function_input(self):
        """This function gets all necessary inputs"""
        member_list = []
        next_member = True
        while next_member:
            sign = input("\nPlease input the sign of the first member, "
                         "it should be either '-' or '+': ")
            if sign not in ("-", "+"):
                print("Please enter correct value")
                continue
            try:
                coefficient = int(
                    input("\nPlease input the coefficient"
                          " of the member which should be a positive number "
                          "(Negativity is decided by the "
                          "sign you inputted previously): "))
                if coefficient <= 0:
                    print("Please start over and enter correct value")
                    continue
            except ValueError:
                print("Please start over and enter numeric value")
                continue
            try:
                power = int(
                    input("\nPlease enter the "
                          "power of 'x' in this member: "))
            except ValueError:
                print("Please start over and enter numeric value")
                continue
            if self.y_choice == "y":
                try:
                    power_y = int(
                        input("\nPlease enter "
                              "the power of 'y' in this member: "))
                except ValueError:
                    print("Please start over and enter numeric value")
                    continue
            else:
                power_y = "-"
            temp_arr = [sign, coefficient, power, power_y]
            member_list.append(temp_arr)
            next_member_choice = input("\nPlease enter "
                                       "'1' if you want to input one more "
                                       "member and '0' to "
                                       "proceed with calculations: ")
            if next_member_choice == "1":
                pass
            elif next_member_choice == "0":
                next_member = False
            else:
                print("Please start over and enter correct value")
                continue
        return member_list

    def integration_by_midpoint(self):
        """This function calculates the result using
        midpoint numerical method"""
        while True:
            try:
                sum_number = int(input("Please input the "
                                       "number of summations. "
                                       "(Bigger number = "
                                       "more precise calculations): "))
                break
            except ValueError:
                print("\nPlease enter numeric values")
        delta = (self.x_final - self.x_initial) / (sum_number - 1)
        numbers = [self.x_initial + i * delta for i in range(sum_number)]
        if self.y_initial == "=":
            print(numbers)
            total_sum = 0
            print(numbers)
            for number in numbers:
                for i in self.force_function:
                    if i[0] == "-":
                        i[1] *= -1
                    total_sum += i[1] * number ** i[2]
        else:
            delta = (self.x_final - self.x_initial) / (sum_number - 1)
            numbers_y = [self.x_initial + i * delta for i in range(sum_number)]
            total_sum = 0
            print(numbers_y)
            for number in numbers:
                for y in numbers_y:
                    for i in self.force_function:
                        if i[0] == "-":
                            i[1] *= -1
                        total_sum += i[1] * number ** i[2] * y ** i[3]
        return total_sum

    def conservative_force(self):
        """This function calculates the
        result using conservative forces method"""
        final_1 = 0
        final_2 = 0
        for i in range(0, len(self.force_function)):
            if self.force_function[i][0] == "+":
                final_1 += self.force_function[i][1] * \
                           self.x_final ** self.force_function[i][2]
            elif self.force_function[i][0] == "-":
                final_1 -= self.force_function[i][1] * \
                           self.x_final ** self.force_function[i][2]
        for i in range(0, len(self.force_function)):
            if self.force_function[i][0] == "+":
                final_2 += self.force_function[i][1] * \
                           self.x_initial ** self.force_function[i][2]
            elif self.force_function[i][0] == "-":
                final_2 -= self.force_function[i][1] * \
                           self.x_initial ** self.force_function[i][2]
        return final_1 - final_2

    def __repr__(self):
        """This function prints"""
        print("\nThis is the function you entered = ", end="")
        if self.force_function[0][3] == "-":
            for i in range(0, len(self.force_function)):
                print(f" {self.force_function[i][0]} "
                      f"{self.force_function[i][1]}"
                      f"x^{self.force_function[i][2]}", end="")
        else:
            for i in range(0, len(self.force_function)):
                print(f" {self.force_function[i][0]} "
                      f"{self.force_function[i][1]}x^"
                      f"{self.force_function[i][2]}y^"
                      f"{self.force_function[i][3]}", end="")
        return ""

    @staticmethod
    def main():
        """Main"""
        process = psutil.Process(os.getpid())
        print("Amount the memory used in bytes: " +
              str(process.memory_info().rss))
        parser = \
            argparse.ArgumentParser(
                description="\nTo compute the work you "
                            "will be asked to enter this data"
                            "\nPlease input the "
                            "function of the force "
                            "depending on 'x' or/and 'y'."
                            "\nFunction must be "
                            "polynomial type. "
                            "This means that it "
                            "must be in such from:"
                            "\n± A*x^B*y^C ± D*x^E*y^F ± ..., "
                            "where the A, D are "
                            "coefficients and B, C, E, F"
                            " are the power of "
                            "the members,"
                            " ± is either positive"
                            " or negative sign."
                            "\nFirstly you will "
                            "input the sign "
                            "of the member, "
                            "then the coefficient,"
                            "\nthen the power of the 'x', "
                            "then you will be "
                            "asked if you want to input next member."
                            "\nIf no the program "
                            "will proceed to calculations."
                            "You will be asked "
                            "if you want to use midpoint method "
                            "or conservative "
                            "forces to compute the integral.")
        while True:
            try:
                x_initial_coordinate = int(
                    input("Please input the "
                          "initial coordinates of the body: "))
                x_final_coordinates = int(
                    input("Please input the "
                          "final coordinates of the body: "))
                break
            except ValueError:
                print("\nPlease enter numeric values")
        using_y_choice = \
            input("Is your function depending on 'x' only or 'y' too?"
                  "\n(Enter 'y' to input two variable "
                  "function or blank otherwise): ")
        if using_y_choice == "y":
            while True:
                try:
                    y_initial_coordinate = int(
                        input("Please input the "
                              "initial coordinates of the body: "))
                    y_final_coordinate = int(
                        input("Please input the "
                              "final coordinates of the body: "))
                    break
                except ValueError:
                    print("\nPlease enter numeric values")
        else:
            y_initial_coordinate = "="
            y_final_coordinate = "="
        body = Body(using_y_choice, x_initial_coordinate,
                    x_final_coordinates,
                    y_initial_coordinate,
                    y_final_coordinate)
        print(body)
        while True:
            choice_2 = input("\nPlease choose whether you want to get result"
                             " using midpoint method "
                             "'1' or conservative forces '2': ")
            if choice_2 == "1":
                result = body.integration_by_midpoint()
                break
            elif choice_2 == "2":
                result = body.conservative_force()
                break
            else:
                print("Please enter correct values! Input '1' or '2'.")
        print(f"\n\nThis is the calculated work: {result}")
        process = psutil.Process(os.getpid())
        print("Amount the memory used in bytes: " +
              str(process.memory_info().rss))


if __name__ == "__main__":
    Body.main()
